package figury;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

public class RandomFigure extends Figura
{
    public RandomFigure(Graphics2D buf, int del, int w, int h)
    {

        super(buf,del,w,h);
        Path2D path = new Path2D.Float();

        path.reset();
        boolean isFirst = true;
        for (int points = 0; points < 5; points++) {
            double x = Math.random() * 50;
            double y = Math.random() * 50;

            if (isFirst) {
                path.moveTo(x, y);
                isFirst = false;
            } else {
                path.lineTo(x, y);
            }
        }
        path.closePath();
        shape = path;

        aft = new AffineTransform();
        area = new Area(shape);

    }
}
